#include <iostream>
#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>
#include <swiftpro/position.h>
#include <std_msgs/UInt8.h>

using namespace std;
bool first=1;

int main(int argc, char **argv){
	float position[3];
	swiftpro::position pos;
	
	ros::init(argc, argv, "user_node");
	ros::NodeHandle n;
	

	ros::Publisher 	pub = n.advertise<std_msgs::UInt8>("user_topic", 1);
	ros::Publisher 	pub2 = n.advertise<std_msgs::UInt8>("start_topic", 1);
	
	ros::Duration(1.0).sleep();
	std_msgs::UInt8 i;
	i.data =1;
	pub2.publish(i);
	ros::spinOnce();
	
	ros::Rate loop_rate(20);
	
	while (ros::ok()){
		
		if(first){
			ros::Duration(0.2).sleep();
			std_msgs::UInt8 i;
			i.data =1;
			pub2.publish(i);
			ros::spinOnce();
			first=0;
		}
		
		std_msgs::UInt8 turn_forward;
		cout<<"Turn page forward --> type 1"<<endl<<"Turn page backward --> type 0 " << endl
				<<"Project content --> type 2 " << endl;
		int read;
		cin >> read;
		//ROS_INFO("%d", read);
		if (read==0) {
			turn_forward.data = 0;
		}
		else if (read==1) turn_forward.data = 1;
		else if (read==2) turn_forward.data = 2;
		else turn_forward.data=3;
		//ROS_INFO("%d", turn_forward.data);
		pub.publish(turn_forward);
		
		ros::spinOnce();
		loop_rate.sleep();
	}		
	return 0;
}

