
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>
#include <swiftpro/position.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>

using namespace cv;
using namespace std;

const int width_first  = 1680; //computer screen resolution
const int height_first = 1050;
const int width_proj  = 1280; //projector resolution
const int height_proj = 720;
const int comic_pixelsize_x = 200;
const int comic_pixelsize_y = 160;

const float COMPRESSION_CONST =0.25;
bool homography_found=0;
Mat H_cameraProjector(3,3,CV_64FC1);

void vision_callback(const std_msgs::Float64MultiArray& msg)
{
	if (msg.data[9]!=2.0){
		ROS_INFO("Page is being turned, not projected on.");
		return;
	}
//page not recognised
	if (msg.data[0]==0.0){
		ROS_INFO("Page not recognised");
		return;
	}
//homography matrix	
	Mat H_comicCamera(3,3,CV_64FC1);
	H_comicCamera.at<double>(0,0) = msg.data[11];
	H_comicCamera.at<double>(0,1) = msg.data[12];
	H_comicCamera.at<double>(0,2) = msg.data[13];
	H_comicCamera.at<double>(1,0) = msg.data[14];
	H_comicCamera.at<double>(1,1) = msg.data[15];
	H_comicCamera.at<double>(1,2) = msg.data[16];
	H_comicCamera.at<double>(2,0) = msg.data[17];
	H_comicCamera.at<double>(2,1) = msg.data[18];
	H_comicCamera.at<double>(2,2) = msg.data[19];
	
	/*	
		double H_cameraProjector_data[3][3]=
			{{6.952767795004854, 1.846509702344437, -287.1587946611609},
			  {-0.2810625795954628, 9.83990947565899, 8.398656889444698},
			  {0.0001192936005377136, 0.002103566110464818, 1}};
	
	Mat H_cameraProjector(3,3,CV_64FC1, H_cameraProjector_data);
	*/
	
	
//project content
	//the background
	Mat projection(Size(width_proj, height_proj), CV_8UC3, Scalar(0,0,0));
	
	//draw the outline of the comics
	std::vector<Point2f> comic_corners(4);
	comic_corners[0] = Point2f(0.0,0.0);
	comic_corners[1] = Point2f((float)comic_pixelsize_x,0.0);
	comic_corners[2] = Point2f((float)comic_pixelsize_x,(float)comic_pixelsize_y);
	comic_corners[3] = Point2f(0.0,(float)comic_pixelsize_y);
	
	cout<<" comic_corners "<<endl<<" "<<comic_corners<< endl<< endl;
	
	perspectiveTransform(comic_corners, comic_corners, H_comicCamera);
	//cout<<" H_comicCamera "<<endl<<" "<<H_comicCamera<< endl<< endl;
	//cout<<" comic_corners "<<endl<<" "<<comic_corners<< endl<< endl;
	std::vector<Point2f> proj_corners;
	perspectiveTransform(comic_corners, proj_corners, H_cameraProjector);
	//cout<<" H_cameraProjector "<<endl<<" "<<H_cameraProjector<< endl<< endl;
	//cout<<" proj_corners "<<endl<<" "<<proj_corners<< endl<< endl;

	line(projection, proj_corners[0], proj_corners[1], Scalar(255,255,255),20);
	line(projection, proj_corners[1], proj_corners[2], Scalar(255,255,255),20);
	line(projection, proj_corners[2], proj_corners[3], Scalar(255,255,255),15);
	line(projection, proj_corners[3], proj_corners[0], Scalar(255,255,255),20);
	
	// add other page-specific content and project (tbd)
		std::vector<Point2f> bubble_centers_46(10);
		std::vector<Point2f> bubble_centers_48(10);
		
		std::vector<Point2f> proj_centers_46(10);
		std::vector<Point2f> proj_centers_48(10);
		
		double alpha = atan((msg.data[3]-msg.data[1])/(2.0*170));  //rotation of the comic, comparing
		//alpha = -alpha*180/M_PI/2; //half rotation in degrees
		alpha = -alpha*180/M_PI;
		
		switch ( (int)msg.data[10] ) {
		  case 46:	
		  	  	  	bubble_centers_46[0] = Point2f(18,17);
		  	  	  	bubble_centers_46[1] = Point2f(88,60);
		  	  	  	bubble_centers_46[2] = Point2f(120,25);
		  	  	  	
		  	  		perspectiveTransform(bubble_centers_46, bubble_centers_46, H_comicCamera);
		  	  		perspectiveTransform(bubble_centers_46, proj_centers_46, H_cameraProjector);
		  	  		
		  	  		circle(projection,proj_centers_46[0],40, Scalar(255,255,255),-1);
		  	  		ellipse(projection, proj_centers_46[1], Size(45,25), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
		  	  		ellipse(projection, proj_centers_46[2], Size(100,70), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
		  	  	
			  	  	namedWindow("projection",CV_WINDOW_NORMAL);
					moveWindow("projection", width_first, 0);
					setWindowProperty("projection", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
					imshow("projection",projection);
					waitKey(12000);
					projection.setTo(Scalar(255,20,147));
					imshow("projection",projection);
					waitKey(30);
					destroyWindow("projection");
			  	  	break;

		  case 48: 	
			  	  	bubble_centers_48[0] = Point2f(25,16);
					bubble_centers_48[1] = Point2f(58,108);
					bubble_centers_48[2] = Point2f(165,59);
					
					perspectiveTransform(bubble_centers_48, bubble_centers_48, H_comicCamera);
					perspectiveTransform(bubble_centers_48, proj_centers_48, H_cameraProjector);
					
					ellipse(projection, proj_centers_48[0], Size(70,40), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
					ellipse(projection, proj_centers_48[1], Size(55,40), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
					ellipse(projection, proj_centers_48[2], Size(80,50), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
		  
		  
		  
			  	  	namedWindow("projection",CV_WINDOW_NORMAL);
					moveWindow("projection", width_first, 0);
					setWindowProperty("projection", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
					imshow("projection",projection);
					waitKey(30000);
					projection.setTo(Scalar(255,20,147));
					imshow("projection",projection);
					waitKey(30);
					destroyWindow("projection");
			  	  	break;

		  default : ROS_INFO("Page %f not in the database for projections",
				  	  msg.data[10]);
		}
	
}


void find_homography(Mat & homo_cam_to_proj){
	
	ros::Duration(4.5).sleep(); //wait for the arm to move out of way
	
	
	//create the checkerboard
	Mat pattern_data(Size( width_proj,height_proj), CV_8UC3, Scalar(255,255,255));
	vector<Point2f> proj_points;

	int quad_size=85;
	int chess_board_width = 12;
	int chess_board_height = 7;
	int x_offset = 0;
	int y_offset = 0;
	Vec3b black=(0,0,0);

	int w_start = (width_proj-(chess_board_width+1)*quad_size)/2 + x_offset;
	int h_start = (height_proj-(chess_board_height+1)*quad_size)/2 + y_offset;
	int min_x = w_start + quad_size;
	int min_y = h_start + quad_size;
	int max_x = w_start + chess_board_width * quad_size;
	int max_y = h_start + chess_board_height * quad_size;

	for (int i = 0; i < chess_board_height+1; i++)
	{
		for (int j = 0; j < chess_board_width+1; j++)
		{
			int x = (w_start + j * quad_size);
			int y = (h_start + i * quad_size);
			// store 2D reference points
			if (i > 0 && j > 0)
			{
				proj_points.push_back(Point2f((float)x, (float)y));
			}
			// save checkerboard
			if ((i + j) % 2)
			{
				for (int ii = x; ii < x+quad_size; ++ii)
				{
					for (int jj = y; jj < y+quad_size; ++jj)
					{
						if (ii < (int)width_proj && jj < (int)height_proj)
						{
							pattern_data.at<Vec3b>(jj,ii) = black;
						}
					}
				}
			}
		}
	}
	
	
//set up the camera and take a picture
	
	VideoCapture cap(0); // open the default camera
		if(!cap.isOpened()){// check if we succeeded
		
			printf("Camera not found");
			return;
		}
		cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
		cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

		cap.set(CV_CAP_PROP_FPS,10);
		cap.set(CV_CAP_PROP_FRAME_WIDTH,1920);
		cap.set(CV_CAP_PROP_FRAME_HEIGHT,1080);
		
		cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
		cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

		//display the checkerboard
		namedWindow("chessboard",CV_WINDOW_NORMAL);
		moveWindow("chessboard", width_first, 0);
		setWindowProperty("chessboard", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
		imshow("chessboard",pattern_data);
		waitKey(50);
		
		Mat cam_image;
		cap >> cam_image; // get a new frame from cameraK
		
		Mat projection(Size(width_proj, height_proj), CV_8UC3, Scalar(255,20,147));
		namedWindow("projection",CV_WINDOW_NORMAL);
		moveWindow("projection", width_first, 0);
		setWindowProperty("projection", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
		imshow("projection",projection);
		waitKey(30);
		destroyWindow("projection");
			
		resize(cam_image, cam_image, Size(), COMPRESSION_CONST, COMPRESSION_CONST, CV_INTER_AREA);
		

	//namedWindow("image",CV_WINDOW_AUTOSIZE);
	//imshow("image",cam_image);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers.png",cam_image);
	//waitKey(0);
	//destroyWindow("image");
	//destroyWindow("chessboard");

	
	//cout<<" proj_points "<<endl<<" "<<proj_points<< endl << endl;
	drawChessboardCorners(pattern_data, Size(chess_board_width,chess_board_height), proj_points,true);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers_drawn1.png",pattern_data);
	
//find checkers on the camera image
	vector<Point2f> cam_points;
	bool found=findChessboardCorners(cam_image, Size(chess_board_width,chess_board_height), cam_points,
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);

	//cout<<" cam_points "<<endl<<" "<<cam_points<< endl<< endl;
	
	
	vector<Point2f> cam_points_help(cam_points.size());
	for(int i=cam_points.size(); i>0;i--){
		cam_points_help[cam_points.size()-i]=cam_points[i-1]; 
	}
	cam_points=cam_points_help;
	//cout<<" cam_points "<<endl<<" "<<cam_points<< endl<< endl;
	
	
	drawChessboardCorners(cam_image, Size(chess_board_width,chess_board_height), cam_points,found);
	//namedWindow("image",CV_WINDOW_AUTOSIZE);
	//imshow("image",cam_image);
	imwrite("/home/iveta/realsense_ws/src/comicbot/images/image_checkers_drawn2.png",cam_image);
	//waitKey(0);
	//destroyWindow("image");
	
//calculate homography


	homo_cam_to_proj = findHomography(cam_points, proj_points);
	cout<<" homo_cam_to_proj "<<endl<<" "<<homo_cam_to_proj<< endl<< endl;
	homography_found=1;
	
}



int main(int argc, char **argv){
	ros::init(argc, argv, "projection_node");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("vision_topic", 1, vision_callback);
	ros::Rate loop_rate(20);
	
	find_homography(H_cameraProjector);
	if (!homography_found) cout<< "Error with projector calibration!"<< endl;
	
	while (ros::ok()){
		ros::spinOnce();
		loop_rate.sleep();
	}		
	return 0;
}

