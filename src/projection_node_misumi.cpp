
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>
#include <swiftpro/position.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>

using namespace cv;
using namespace std;

const int width_first  = 1680; //computer screen resolution
const int height_first = 1050;
const int width_proj  = 1280; //projector resolution
const int height_proj = 720;
const int comic_pixelsize_x = 200;
const int comic_pixelsize_y = 160;

void vision_callback(const std_msgs::Float64MultiArray& msg)
{
//no projection, page is being turned
	if (msg.data[9]!=2.0){
			ROS_INFO("No projection, page is being turned");
			return;
		}
//page not recognised
	if (msg.data[0]==0.0){
		ROS_INFO("Page not recognised");
		return;
	}
//homography matrix	
	Mat H_comicCamera(3,3,CV_64FC1);
	H_comicCamera.at<double>(0,0) = msg.data[11];
	H_comicCamera.at<double>(0,1) = msg.data[12];
	H_comicCamera.at<double>(0,2) = msg.data[13];
	H_comicCamera.at<double>(1,0) = msg.data[14];
	H_comicCamera.at<double>(1,1) = msg.data[15];
	H_comicCamera.at<double>(1,2) = msg.data[16];
	H_comicCamera.at<double>(2,0) = msg.data[17];
	H_comicCamera.at<double>(2,1) = msg.data[18];
	H_comicCamera.at<double>(2,2) = msg.data[19];
	
	double H_cameraProjector_data[3][3]=
			{{7.558569315934224, 1.368147260910606, -1173.439180238532},
			  {0.1528202175655223, 12.08136919446677, -1428.423024211796},
			  {-0.001138169139293756, 0.005725540774476954, 1}};
	
	Mat H_cameraProjector(3,3,CV_64FC1, H_cameraProjector_data);
	
//project content
	//the background
	Mat projection(Size(width_proj, height_proj), CV_8UC3, Scalar(0,0,0));
	
	//draw the outline of the comics
	std::vector<Point2f> comic_corners(4);
	comic_corners[0] = Point2f(0.0,0.0);
	comic_corners[1] = Point2f((float)comic_pixelsize_x,0.0);
	comic_corners[2] = Point2f((float)comic_pixelsize_x,(float)comic_pixelsize_y);
	comic_corners[3] = Point2f(0.0,(float)comic_pixelsize_y);
	
	//cout<<" comic_corners "<<endl<<" "<<comic_corners<< endl<< endl;
	
	perspectiveTransform(comic_corners, comic_corners, H_comicCamera);
	//cout<<" H_comicCamera "<<endl<<" "<<H_comicCamera<< endl<< endl;
	//cout<<" comic_corners "<<endl<<" "<<comic_corners<< endl<< endl;
	std::vector<Point2f> proj_corners;
	perspectiveTransform(comic_corners, proj_corners, H_cameraProjector);
	//cout<<" H_cameraProjector "<<endl<<" "<<H_cameraProjector<< endl<< endl;
	//cout<<" proj_corners "<<endl<<" "<<proj_corners<< endl<< endl;

	line(projection, proj_corners[0], proj_corners[1], Scalar(255,255,255),25);
	line(projection, proj_corners[1], proj_corners[2], Scalar(255,255,255),25);
	line(projection, proj_corners[2], proj_corners[3], Scalar(255,255,255),20);
	line(projection, proj_corners[3], proj_corners[0], Scalar(255,255,255),25);
	
	// add other page-specific content and project (tbd)
		std::vector<Point2f> bubble_centers_46(10);
		std::vector<Point2f> bubble_centers_48(10);
		
		std::vector<Point2f> proj_centers_46(10);
		std::vector<Point2f> proj_centers_48(10);
		
		double alpha = atan((msg.data[3]-msg.data[1])/(2.0*170));  //rotation of the comic, comparing
		alpha = alpha*180/M_PI/2; //half rotation in degrees
		
		switch ( (int)msg.data[10] ) {
		  case 46:	
		  	  	  	bubble_centers_46[0] = Point2f(15,17);
		  	  	  	bubble_centers_46[1] = Point2f(88,60);
		  	  	  	bubble_centers_46[2] = Point2f(120,20);
		  	  	  	
		  	  		perspectiveTransform(bubble_centers_46, bubble_centers_46, H_comicCamera);
		  	  		perspectiveTransform(bubble_centers_46, proj_centers_46, H_cameraProjector);
		  	  		
		  	  		circle(projection,proj_centers_46[0],50, Scalar(255,255,255),-1);
		  	  		ellipse(projection, proj_centers_46[1], Size(40,30), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
		  	  		ellipse(projection, proj_centers_46[2], Size(90,50), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
		  	  	
		  	  		cout<<"Page-specific projection is beeing started."<< endl;
			  	  	namedWindow("projection",CV_WINDOW_NORMAL);
					moveWindow("projection", width_first, 0);
					setWindowProperty("projection", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
					imshow("projection",projection);
					waitKey(20000);
					cout<<"Page-specific projection has been ended "<< endl;
					projection.setTo(Scalar(255,20,147));
					imshow("projection",projection);
					waitKey(30);
					destroyWindow("projection");
			  	  	break;

		  case 48: 	
			  	  	bubble_centers_48[0] = Point2f(25,16);
					bubble_centers_48[1] = Point2f(58,108);
					bubble_centers_48[2] = Point2f(166,58);
					
					perspectiveTransform(bubble_centers_48, bubble_centers_48, H_comicCamera);
					perspectiveTransform(bubble_centers_48, proj_centers_48, H_cameraProjector);
					
					ellipse(projection, proj_centers_48[0], Size(55,40), 0, 0.0, 360.0, Scalar(255,255,255),-1);
					ellipse(projection, proj_centers_48[1], Size(45,35), 0, 0.0, 360.0, Scalar(255,255,255),-1);
					ellipse(projection, proj_centers_48[2], Size(72,32), alpha, 0.0, 360.0, Scalar(255,255,255),-1);
		  
		  
					cout<<"Page-specific projection is beeing started."<< endl;
			  	  	namedWindow("projection",CV_WINDOW_NORMAL);
					moveWindow("projection", width_first, 0);
					setWindowProperty("projection", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
					imshow("projection",projection);
					waitKey(20000);
					cout<<"Page-specific projection has been ended "<< endl;
					projection.setTo(Scalar(255,20,147));
					imshow("projection",projection);
					waitKey(30);
					destroyWindow("projection");
			  	  	break;

		  default : ROS_INFO("Page %f not in the database for projections",
				  	  msg.data[10]);
		}
	
}



int main(int argc, char **argv){
	ros::init(argc, argv, "projection_node");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("vision_topic", 1, vision_callback);
	ros::Rate loop_rate(20);
	
	while (ros::ok()){
		ros::spinOnce();
		loop_rate.sleep();
	}		
	return 0;
}

