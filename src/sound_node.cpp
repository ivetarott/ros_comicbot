
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>
#include <swiftpro/position.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>
#include <sound_play/sound_play.h>
#include <sound_play/SoundRequest.h>
#include <unistd.h>

using namespace cv;
using namespace std;

int user_input=10;
const char *str = "";
const char *str_info = "";
bool bee=0;
float wait;
/*
 * Plays the bee sound while the arm is moving.
 * "roslaunch sound_play soundplay_node.launch" has to be launched before sound_node
 * 
 * 
 * 
 * 
 */

void start_callback(const std_msgs::UInt8& msg)
{
	str = "/home/iveta/comicbot_ws/src/ros_comicbot/sound/hellomyfriend.wav";
	str_info = "Welcome sound will be played.";
	wait = 5.0;
}



void user_callback(const std_msgs::UInt8& msg)
{
	if ((msg.data==0)||(msg.data==1)){
		
			str_info= "Ill turn the page for you sound will be played.";
			str = "/home/iveta/comicbot_ws/src/ros_comicbot/sound/okillturn.wav";
			bee=1;
			wait = 4.0;
		}
	
}


void vision_callback(const std_msgs::UInt8& msg)
{
	sound_play::SoundClient sc;
	if (msg.data==1){
		str_info= "New page sound will be played.";
		str = "/home/iveta/comicbot_ws/src/ros_comicbot/sound/lookwhatwehave.wav";
		wait = 3.0;
	}
	else if(msg.data==2){
		str_info="Ill try once again sound will be played.";
		str = "/home/iveta/comicbot_ws/src/ros_comicbot/sound/somethingwentwrong.wav";
		bee=1;
		wait = 6.0;
	}
	else if(msg.data==3){
		str_info= "Il cant turn the page sound will be played.";
		str = "/home/iveta/comicbot_ws/src/ros_comicbot/sound/icantturn.wav";
		wait = 6.0;
	}
}


int main(int argc, char **argv){
	ros::init(argc, argv, "sound_node");
	ros::NodeHandle nh;

	sound_play::SoundClient sc;
	
	ros::Subscriber sub = nh.subscribe("user_topic", 1, user_callback);
	ros::Subscriber sub2 = nh.subscribe("start_topic", 1, start_callback);
	ros::Subscriber sub3 = nh.subscribe("vision_topic_sound", 1, vision_callback);
	ros::Rate loop_rate(20);
	
	while (ros::ok()){
		
		
		if(str!=""){ //some text message will be played
			cout<< str_info<< endl;
			sc.playWave(str);
			ros::Duration(wait).sleep();
			str="";
		}
		if(bee){
			const char *bee_str = "/home/iveta/comicbot_ws/src/ros_comicbot/sound/bumblebee.wav";
			cout<< "Bee sound will be played"<< endl;
			sc.startWave(bee_str);
			ros::Duration(6.0).sleep();
			sc.stopWave(bee_str);
			bee=0;	
		}
	
		ros::spinOnce();
		loop_rate.sleep();
	}		
	return 0;
}

