

#include <math.h>
#include <stdlib.h>
#include <string>
#include <ros/ros.h>
#include <serial/serial.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <swiftpro/SwiftproState.h>
#include <swiftpro/status.h>
#include <swiftpro/position.h>
#include <swiftpro/angle4th.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>

using namespace std;

bool turned=0;
serial::Serial _serial;				// serial object
std::string Gcode = "";
std_msgs::String result;
char x[10];
char y[10];
char z[10];
char m4[10];


void vision_callback(const std_msgs::Float64MultiArray& msg)
{

	if (msg.data[0]==0.0){
		ROS_INFO("Page not recognised by the vision system.");
		return;
	}
	if (msg.data[9]==2.0){
		ROS_INFO("Page not being turned, page being projected on.");
		return;
	}

	double x1,x2,x3,y1,y2,y3, alpha,z_pick, temp;
	
	alpha=0;
	alpha = atan((msg.data[3]-msg.data[1])/(2.0*170));  //rotation of the comic, comparing corners 0 and 1
	if (fabs(alpha)> 0.52 ){ // >30 degrees
		ROS_INFO("alpha too big");
		return; 
	}
	//z_pick = 57.0;

	if (msg.data[20]>1) ros::Duration(6.5).sleep(); //wait till the talkin is done
		
		
//turn the page forward	
	if (msg.data[9]==1.0){
		ROS_INFO("Turning page forward");
		if (msg.data[20]>2) ROS_INFO("Turning page forward - third or more attempt");
		if (msg.data[10]<50){				//determine the z-coordinate for picking the page depending on the page number
					z_pick=57.0; //in theory should be more
				}
				else {
					z_pick=55.0;
				}
				
				
		//checking an acceptable position
		if ( ((msg.data[3]>220) || (msg.data[3]<60)) || ((msg.data[4]>250) || (msg.data[4]<70)) ){
			ROS_INFO("Coordinates of corner 1 out of position! Move the book in front of the arm!");
		return;
		}
		
		
		x3=msg.data[3]; //initialise the coordinates of the corner 1 (top right)
		y3=msg.data[4];
		
		//go to the picking x,y position
		x1=80;
		y1= -85;//picking point of the top right corner x1 mm from top and y1 mm from right
		if (msg.data[20]>1) y1=-65;		//at third+ attempt, try a different position for picking
		x2=y1*sin(alpha)+x1*cos(alpha); //adjust to the angle of rotation of the book
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f",x3);
		sprintf(y, "%.2f", y3	);
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(1.0).sleep(); //wait (in seconds)
		
		
		//pick page
		sprintf(z, "%.2f", z_pick);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(1.0).sleep();
		
		
		Gcode = (std::string)"M2231 V1\n"; //turn on the pump
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());	
		ros::Duration(1.0).sleep();	
		
		x1=0;
		y1=-9;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		sprintf(z, "%.2f", 90.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F4000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(0.5).sleep();
		
		//turn the page
		x1=0;
		y1=-270;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		sprintf(z, "%.2f", 105.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F20000" + "\r\n"; //the speed is increased
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(2.0).sleep();
		
		Gcode = (std::string)"M2231 V0\n"; //turn off the pump
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());	
		ros::Duration(0.5).sleep();	
	
		//go back to starting position		
		sprintf(x, "%.2f", 52.0);
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 75.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());	
		ros::Duration(1.5).sleep();

		//the turning is done
		turned=1;
	}
//turning the page backwards	
	else if (msg.data[9]==0.0){
		ROS_INFO("Turning page backwards");
		if (msg.data[20]>2) ROS_INFO("Turning page backwards - third or more attempt");
		if (msg.data[10]<50){				//determine the z-coordinate for picking the page depending on the page number
							z_pick=55.0;
						}
						else {
							z_pick=57.0; //in theory should be more
						}
		
		 if ( ((msg.data[1]>220) || (msg.data[1]<60)) ||((msg.data[2]>-70) || (msg.data[2]<-250)) ) {
			ROS_INFO("Coordinates of corner 0 out of position! Move the book in front of the arm!");
				return;			
		}

		x3=msg.data[1]; //initialise the position of corner 0 (top left)
		y3=msg.data[2];
		
		//go to the picking x,y position
		x1=80;		//picking point of the top left corner
		y1=75;       //wrong calibration, is the same as -85 on the other side..    
		//y1=85;
		if (msg.data[20]>1) x1=65;		//at third+ attempt, try a different position for picking
		x2=y1*sin(alpha)+x1*cos(alpha); //adjust to the angle of rotation of the book
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f",x3);
		sprintf(y, "%.2f", y3	);
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(1.0).sleep(); //wait (in seconds)
		
		
		//pick page
		sprintf(z, "%.2f", z_pick);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(1.0).sleep();
		
		
		Gcode = (std::string)"M2231 V1\n"; //turn on the pump
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());	
		ros::Duration(1.0).sleep();	
		
		x1=0;
		y1=9;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		sprintf(z, "%.2f", 90.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F4000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(0.5).sleep();
		
		//turn the page
		x1=0;
		y1=270;
		if (msg.data[20]>2) y1=300;		//at third+ attempt,page must be pulled further because of the picking position
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		sprintf(z, "%.2f", 105.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F20000" + "\r\n"; //the speed is increased
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(2.0).sleep();
		
		Gcode = (std::string)"M2231 V0\n"; //turn off the pump
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());	
		ros::Duration(0.5).sleep();	
	
		//go back to starting position		
		sprintf(x, "%.2f", 52.0);
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 75.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());	
		ros::Duration(1.5).sleep();

		//the turning is done
		turned=1;

	}
	else if (msg.data[9]==2.0){
		ROS_INFO("Page is being projected on.");
	}
	else{
		ROS_INFO("There has been wrong user input. Type 1 to turn page forward, type 0 to turn page backward.");
	}
}



int main(int argc, char** argv){	
	ros::init(argc, argv, "move_node");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("vision_topic", 1, vision_callback);
	ros::Publisher 	pub = nh.advertise<std_msgs::Bool>("move_topic", 1);
	ros::Rate loop_rate(20);

	try{
		_serial.setPort("/dev/ttyACM0");
		_serial.setBaudrate(115200);
		serial::Timeout to = serial::Timeout::simpleTimeout(1000);
		_serial.setTimeout(to);
		_serial.open();
		ROS_INFO_STREAM("Port has been open successfully");
	}
	catch (serial::IOException& e){
		ROS_ERROR_STREAM("Unable to open port");
		return -1;
	}
	
	if (_serial.isOpen()){
		ros::Duration(3.5).sleep();				// wait 3.5s
		_serial.write("M2120 V0\r\n");			// stop report position
		ros::Duration(0.1).sleep();				// wait 0.1s
		_serial.write("M17\r\n");				// attach
		ros::Duration(0.1).sleep();				// wait 0.1s
		
		//move to the starting position
		sprintf(x, "%.2f", 52.0);				
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 75.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(0.2).sleep();
		
		sprintf(m4, "%.2f", 90.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		ros::Duration(0.2).sleep();
		
		ROS_INFO_STREAM("Attach and wait for commands");
	}
	
	std_msgs::Bool page_turned;
	while (ros::ok()){
		if(turned==1){
			turned=0;
			page_turned.data=1;
			pub.publish(page_turned);
			
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	
	return 0;
}


