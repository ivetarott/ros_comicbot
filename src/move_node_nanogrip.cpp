

#include <math.h>
#include <stdlib.h>
#include <string>
#include <ros/ros.h>
#include <serial/serial.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <swiftpro/SwiftproState.h>
#include <swiftpro/status.h>
#include <swiftpro/position.h>
#include <swiftpro/angle4th.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>

using namespace std;

bool turned=0;
serial::Serial _serial;				// serial object
std::string Gcode = "";
std_msgs::String result;
char x[10];
char y[10];
char z[10];
char m4[10];

/* 
 * Description: callback when receive data from position_write_topic
 * Inputs: 		msg(float)			3 cartesian coordinates: x, y, z(mm)
 * Outputs:		Gcode				send gcode to control swift pro
 */

void vision_callback(const std_msgs::Float64MultiArray& msg)
{

	if (msg.data[0]==0.0){
		ROS_INFO("Page not recognised");
		return; //page not recognised
	}
	if (msg.data[9]==2.0){
		ROS_INFO("Page not being turned, page being projected on");
		return;
	}

	double x1,x2,x3,y1,y2,y3, alpha,z_pick, temp;
	
	alpha=0;
	//alpha = atan((msg.data[3]-msg.data[1])/(2.0*170));  //rotation of the comic, comparing corners 0 and 1
	if (fabs(alpha)> 0.52 ){
		ROS_INFO("alpha too big");
		return; // >30 degrees
	}
	

//turn the page forward	
	if (msg.data[9]==1.0){ 
		ROS_INFO("Turning page forward");
		if (msg.data[10]<34){				//determine the z-coordinate for picking the page depending on the page number
					z_pick=68.0;
				}
				else if (msg.data[10]<67){
					z_pick=66.0;
				}
				else {
					z_pick=64.0;
				}
		
		if ( ((msg.data[3]>220) || (msg.data[3]<60)) || ((msg.data[4]>250) || (msg.data[4]<70)) ){
			ROS_INFO("coordinates of corner 1 out of position");
			ROS_INFO("book to be moved to a better position");
			//to be implemented: moving the comic book to a feasible position instead of "return"
			
			if(msg.data[4]>250){ //book too far right, corner 0 has to be pulled outwards
					ROS_INFO("book too far right, corner 0 has to be pulled outwards");
					
					if (msg.data[10]<34){				//determine the z-coordinate for picking the page depending on the page number
						z_pick=64.0;
					}
					else if (msg.data[10]<67){
						z_pick=66.0;
					}
					else {
						z_pick=68.0;
					}
					
					//go to the corner-grabbing position
					x1=80;
					y1=40;
					x2=y1*sin(alpha)+x1*cos(alpha);
					y2=y1*cos(alpha)-x1*sin(alpha);
					x3=msg.data[1]+x2;
					y3=msg.data[2]+y2;
					sprintf(x, "%.2f",min(x3,320.0));
					sprintf(y, "%.2f", y3	);
					sprintf(z, "%.2f", 100.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.5).sleep();
					
					
					sprintf(z, "%.2f", z_pick);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(2.0).sleep();	//wait (in seconds)
					
					//pull to the ideal position
					x3=170.0;
					y3=-120.0;
					sprintf(x, "%.2f", x3);
					sprintf(y, "%.2f", y3);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(2.5).sleep();
					
					//lift
					
					sprintf(z, "%.2f", 75.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.0).sleep();	//wait (in seconds)
				
					sprintf(m4, "%.2f", 140.0);
					Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.0).sleep();	//wait (in seconds)
					
					//go back
					sprintf(z, "%.2f", 120.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					sprintf(x, "%.2f", 70.0);
					sprintf(y, "%.2f", 0.0);
					sprintf(z, "%.2f", 80.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.5).sleep();
					
					sprintf(m4, "%.2f", 90.0);
					Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.0).sleep();	//wait (in seconds)
					
				}
				else{ 
					//if(msg.data[4]<70){//book too far left, corner 1 has to be pulled outwards
					ROS_INFO("book too far left, corner 1 has to be pulled outwards");
					//go to the corner-grabbing position
					x1=60;
					y1=-30;
					x2=y1*sin(alpha)+x1*cos(alpha);
					y2=y1*cos(alpha)-x1*sin(alpha);
					x3=msg.data[3]+x2;
					y3=msg.data[4]+y2;
					sprintf(x, "%.2f",min(x3,320.0));
					sprintf(y, "%.2f", y3	);
					sprintf(z, "%.2f", 110.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.5).sleep();
					
					
					sprintf(z, "%.2f", z_pick);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.0).sleep();	//wait (in seconds)
					
					//pull to the ideal position
					x3=170;
					y3=120;
					sprintf(x, "%.2f", x3);
					sprintf(y, "%.2f", y3);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(2.5).sleep();
					
					//lift
					
					sprintf(z, "%.2f", 75.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.0).sleep();	//wait (in seconds)
				
					sprintf(m4, "%.2f", 40.0);
					Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.5).sleep();	//wait (in seconds)
					
					//go back
					sprintf(z, "%.2f", 120.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
						
					ros::Duration(2.0).sleep();	
					
					sprintf(x, "%.2f", 70.0);
					sprintf(y, "%.2f", 0.0);
					sprintf(z, "%.2f", 80.0);
					Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.5).sleep();
					
					sprintf(m4, "%.2f", 90.0);
					Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
					ROS_INFO("%s", Gcode.c_str());
					_serial.write(Gcode.c_str());
					result.data = _serial.read(_serial.available());
					
					ros::Duration(1.0).sleep();	//wait (in seconds)
					
			}
		return;
		}
		
		x3=msg.data[3];
		y3=msg.data[4];
		sprintf(x, "%.2f", 150.0);
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
	
		x1=60;
		y1=-30;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=msg.data[3]+x2;
		y3=msg.data[4]+y2;
		sprintf(x, "%.2f",x3);
		sprintf(y, "%.2f", y3	);
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
		
		 
		sprintf(m4, "%.2f", 20.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		sprintf(z, "%.2f", z_pick);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(2.5).sleep();	//wait
		
		sprintf(z, "%.2f", 110.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
		
		x1=0;
		y1=-60;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());

		ros::Duration(1.0).sleep();		
		
		sprintf(m4, "%.2f", 60.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(2.5).sleep();	
		
		sprintf(z, "%.2f", 140.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();	//wait 1 second
		
		
		x1=0;
		y1=-90;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		
		sprintf(m4, "%.2f", 160.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(2.0).sleep();
		
		x1=0;
		y1=-270;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", max(-sqrt(320*320-x3*x3)-1,y3)); //maximum reach of the arm is not trespassed
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		/*sprintf(m4, "%.2f", 140.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();*/
		
		sprintf(z, "%.2f", 160.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());		
		
		ros::Duration(1.5).sleep();	//wait
		
		sprintf(x, "%.2f", 70.0);
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 90.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
		
		sprintf(m4, "%.2f", 90.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
	
		turned=1;
	}
//turning the page backwards	
	else if (msg.data[9]==0.0){
		ROS_INFO("Turning page backwards");
		if (msg.data[10]<34){				//determine the z-coordinate for picking the page depending on the page number
				z_pick=64.0;
			}
			else if (msg.data[10]<67){
				z_pick=66.0;
			}
			else {
				z_pick=68.0;
			}
		if ( ((msg.data[1]>220) || (msg.data[1]<60)) ||((msg.data[2]>-70) || (msg.data[2]<-250)) ) {
			ROS_INFO("coordinates of corner 0 out of feasible position");
		
			
			if(msg.data[2]>-70){ //book too far right, corner 0 has to be pulled outwards
						ROS_INFO("book too far right, corner 0 has to be pulled outwards");
						
						
						//go to the corner-grabbing position
						x1=80;
						y1=40;
						x2=y1*sin(alpha)+x1*cos(alpha);
						y2=y1*cos(alpha)-x1*sin(alpha);
						x3=msg.data[1]+x2;
						y3=msg.data[2]+y2;
						sprintf(x, "%.2f",min(x3,320.0));
						sprintf(y, "%.2f", y3	);
						sprintf(z, "%.2f", 100.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.5).sleep();
						
						
						sprintf(z, "%.2f", z_pick);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(2.0).sleep();	//wait (in seconds)
						
						//pull to the ideal position
						x3=170.0;
						y3=-120.0;
						sprintf(x, "%.2f", x3);
						sprintf(y, "%.2f", y3);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(2.5).sleep();
						
						//lift
						
						sprintf(z, "%.2f", 75.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.0).sleep();	//wait (in seconds)
					
						sprintf(m4, "%.2f", 140.0);
						Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.0).sleep();	//wait (in seconds)
						
						//go back
						sprintf(z, "%.2f", 120.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.5).sleep();
						
						sprintf(x, "%.2f", 70.0);
						sprintf(y, "%.2f", 0.0);
						sprintf(z, "%.2f", 80.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.5).sleep();
						
						sprintf(m4, "%.2f", 90.0);
						Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.0).sleep();	//wait (in seconds)
						
					}
					else{ 
						//if(msg.data[2]<-250){//book too far left, corner 1 has to be pulled outwards
						ROS_INFO("book too far left, corner 1 has to be pulled outwards");
						if (msg.data[10]<34){				//determine the z-coordinate for picking the page depending on the page number
							z_pick=68.0;
						}
						else if (msg.data[10]<67){
							z_pick=66.0;
						}
						else {
							z_pick=64.0;
						}
						
						//go to the corner-grabbing position
						x1=60;
						y1=-30;
						x2=y1*sin(alpha)+x1*cos(alpha);
						y2=y1*cos(alpha)-x1*sin(alpha);
						x3=msg.data[3]+x2;
						y3=msg.data[4]+y2;
						sprintf(x, "%.2f",min(x3,320.0));
						sprintf(y, "%.2f", y3	);
						sprintf(z, "%.2f", 100.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.5).sleep();
						
						
						sprintf(z, "%.2f", z_pick);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.0).sleep();	//wait (in seconds)
						
						//pull to the ideal position
						x3=170;
						y3=120;
						sprintf(x, "%.2f", x3);
						sprintf(y, "%.2f", y3);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(2.5).sleep();
						
						//lift
						
						sprintf(z, "%.2f", 75.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.0).sleep();	//wait (in seconds)
					
						sprintf(m4, "%.2f", 40.0);
						Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(2.5).sleep();	//wait (in seconds)
						
						//go back
						sprintf(z, "%.2f", 120.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
							
						ros::Duration(2.0).sleep();	
						
						sprintf(x, "%.2f", 70.0);
						sprintf(y, "%.2f", 0.0);
						sprintf(z, "%.2f", 80.0);
						Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.5).sleep();
						
						sprintf(m4, "%.2f", 90.0);
						Gcode = (std::string)"G2202 N3 V" + m4 + " F0" +"\r\n";
						ROS_INFO("%s", Gcode.c_str());
						_serial.write(Gcode.c_str());
						result.data = _serial.read(_serial.available());
						
						ros::Duration(1.0).sleep();	//wait (in seconds)
						
					}
				return;		
			
			

		}

		//get the arm to the starting position
		sprintf(x, "%.2f", 150.0);
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
	
		x1=80;
		y1=40;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=msg.data[1]+x2;
		y3=msg.data[2]+y2;
		sprintf(x, "%.2f",x3);
		sprintf(y, "%.2f", y3	);
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		
		sprintf(m4, "%.2f", 140);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		sprintf(z, "%.2f", z_pick);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(2.5).sleep();	//wait 2 seconds
		
		sprintf(z, "%.2f", 100.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
		
		x1=0;
		y1=60;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());

		ros::Duration(1.0).sleep();		
		
		sprintf(m4, "%.2f", 120);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		sprintf(z, "%.2f", 140.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();	//wait 1 second
		
		
		x1=0;
		y1=88;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", y3);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
		
		
		sprintf(m4, "%.2f", 80);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		x1=0;
		y1=270;
		x2=y1*sin(alpha)+x1*cos(alpha);
		y2=y1*cos(alpha)-x1*sin(alpha);
		x3=x3+x2;
		y3=y3+y2;
		sprintf(x, "%.2f", x3);
		sprintf(y, "%.2f", min(sqrt(320*320-x3*x3)-1,y3));
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
		
		sprintf(m4, "%.2f", 40.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		
		sprintf(z, "%.2f", 160.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());		
		
		ros::Duration(1.5).sleep();	//wait
		
		sprintf(x, "%.2f", 70.0);
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 90.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(0.5).sleep();
	
		sprintf(m4, "%.2f", 90.0);
		Gcode = (std::string)"G2202 N3 V" + m4 + " F0" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ros::Duration(1.5).sleep();
		
		turned=1;

	}
	else{
		ROS_INFO("Not correct user input for picking the page! Type 1 to turn page forward, type 2 to turn page backward");
	}
	
}



int main(int argc, char** argv){	
	ros::init(argc, argv, "move_node");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("vision_topic", 1, vision_callback);
	ros::Publisher 	pub = nh.advertise<std_msgs::Bool>("move_node_topic", 1);
	ros::Rate loop_rate(20);

	try{
		_serial.setPort("/dev/ttyACM0");
		_serial.setBaudrate(115200);
		serial::Timeout to = serial::Timeout::simpleTimeout(1000);
		_serial.setTimeout(to);
		_serial.open();
		ROS_INFO_STREAM("Port has been open successfully");
	}
	catch (serial::IOException& e){
		ROS_ERROR_STREAM("Unable to open port");
		return -1;
	}
	
	if (_serial.isOpen()){
		ros::Duration(3.5).sleep();				// wait 3.5s
		_serial.write("M2120 V0\r\n");			// stop report position
		ros::Duration(0.1).sleep();				// wait 0.1s
		_serial.write("M17\r\n");				// attach
		ros::Duration(0.1).sleep();				// wait 0.1s
		
		//move to the starting position
		sprintf(x, "%.2f", 70.0);				
		sprintf(y, "%.2f", 0.0);
		sprintf(z, "%.2f", 90.0);
		Gcode = (std::string)"G0 X" + x + " Y" + y + " Z" + z + " F10000" + "\r\n";
		ROS_INFO("%s", Gcode.c_str());
		_serial.write(Gcode.c_str());
		result.data = _serial.read(_serial.available());
		
		ROS_INFO_STREAM("Attach and wait for commands");
	}
	
	std_msgs::Bool page_turned;
	while (ros::ok()){
		if(turned=1){
			page_turned.data=turned;
			pub.publish(page_turned);
			turned=0;
		}
		ros::spinOnce();
		loop_rate.sleep();
	}
	
	return 0;
}


