/*
 * main.cpp
 *
 *  Created on: Aug 2, 2017
 *      Author: iveta
 */

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <keyboard/Key.h>

#include <librealsense/rs.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <iostream>
#include <eigen3/Eigen/Dense>
#include <math.h>

#include <string>
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/JointState.h>
#include <swiftpro/position.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt8.h>

using namespace cv;
using namespace std;

int user_input;
bool status=0;
float corners[4][2];
double H_ROS[9];
int page;
int attempt=1;
int sound_output=0;


//constants for matching pages
const float COMPRESSION_CONST =0.25;
const int NUMBER_PAGES = 51;
const float MAXIMUM_MATCHES_DIFFERENCE = 0.75; //second best page pick can have maximum
											//MAXIMUM_MATCHES_DIFFERENCE*100% of matches
											//of the first pick

//camera intrinsic - fisheye
const float ALPHA_X=170.97336729652767 * 1920.0/640.0;
const float ALPHA_Y=170.97336729652767 * 1440.0/480.0  ;
const float U_0=319.5 * 1920.0/640.0;
const float V_0=239.5  * 1440.0/480.0;
const float CAM_GAMMA=0.0;
const float DIST_COEFF_ARRAY[5]= {-0.16641737217844535, 0.023447069536659728,
		   -0.0024857075525880493, 0.0014544985735037901,
		   -0.0012732242148860962};

//compares the descriptors against a database
void recognize_page(Mat descriptors, int &best_page,vector<KeyPoint> &keypoints2,
		vector< DMatch > &good_matches_final){
	best_page=0;
	int most_matches=0;
	int second_most_matches=0;
	FileStorage fs("/home/iveta/comicbot_ws/src/comicbot/descriptors/comics_descriptors.yml", FileStorage::READ);

	for(int i=1; i<= NUMBER_PAGES; i++){

		//printf("Matching against page number %d \n", i);
		Mat descriptors2;

		string iValue= to_string(i);
		string filename = "descriptors_"+ iValue;
		fs[filename] >> descriptors2;

		//Matching descriptor vectors using FLANN matcher
		FlannBasedMatcher matcher;
		std::vector< DMatch > matches;
		matcher.match(descriptors, descriptors2, matches);

		/* code performs better with using a constant when choosing good matches
		//Calculation of max and min distances between keypoints
		double min_dist = 1000;
		for( int i = 0; i < descriptors.rows; i++ ){
			 double dist = matches[i].distance;
			 if( dist < min_dist ) min_dist = dist;
		}

		printf("-- Min dist : %f \n", min_dist );
		*/

		//Pick only "good" matches
		std::vector< DMatch > good_matches;
		for( int i = 0; i < descriptors.rows; i++ ){
			if( matches[i].distance <= 250 ){				//constant set by testing
				 good_matches.push_back(matches[i]);
			}
		}

		/*
		//Prints number of matches on the currently compared page
		printf("%zd matches have been found with double-page %d. \n",
				good_matches.size(), i);
		*/

		//tracks the page with the most matches
		if (good_matches.size()> most_matches){
			most_matches= good_matches.size();
			best_page=i;
			good_matches_final=good_matches;
		}
		else if (good_matches.size()> second_most_matches){
			second_most_matches= good_matches.size();
		}

	} //end loop over all pages in the database

	//checking the confidence as opposed to second best choice of a page
	if ( ((float)second_most_matches/most_matches) < MAXIMUM_MATCHES_DIFFERENCE){
		//get the keypoints for further analysis
		string iValue = to_string(best_page);
		string filename = "keypoints_"+ iValue;
		FileNode kptNode = fs[filename];
		read(kptNode, keypoints2);
		fs.release(); //close the file with descriptors database
	}
	else
		best_page=0;
	

}


void localize(vector<KeyPoint> keypoints, vector<KeyPoint> keypoints2,
		std::vector< DMatch > good_matches, Mat &img_matches, Mat color2,
		vector<Point2f> &scene_corners){

	vector<Point2f> scene;
	vector<Point2f> book;
	//Get the keypoints from the good matches
	for( int i = 0; i < good_matches.size(); i++ ){
		scene.push_back(keypoints[good_matches[i].queryIdx].pt);
		book.push_back(keypoints2[good_matches[i].trainIdx].pt);
	}
	//cout<<"book "<<endl<<" "<<book<< endl<< endl;
	//cout<<" scene "<<endl<<" "<<scene<< endl<< endl;
	Mat H = findHomography(book, scene, CV_RANSAC);
	
	H_ROS[0]=H.at<double>(0,0);
	H_ROS[1]=H.at<double>(0,1);
	H_ROS[2]=H.at<double>(0,2);
	H_ROS[3]=H.at<double>(1,0);
	H_ROS[4]=H.at<double>(1,1);
	H_ROS[5]=H.at<double>(1,2);
	H_ROS[6]=H.at<double>(2,0);
	H_ROS[7]=H.at<double>(2,1);
	H_ROS[8]=H.at<double>(2,2);
	

	//Get the corners from the book
	std::vector<Point2f> object_corners(4);
	object_corners[0] = cvPoint(0.0,0.0);
	object_corners[1] = cvPoint(color2.cols,0.0);
	object_corners[2] = cvPoint(color2.cols,color2.rows);
	object_corners[3] = cvPoint(0.0,color2.rows);


	perspectiveTransform(object_corners, scene_corners, H);
	//cout<<" object_corners "<<endl<<" "<<object_corners<< endl<< endl;
	//cout<<" scene_corners "<<endl<<" "<<scene_corners<< endl<< endl;
	//cout<<" H "<<endl<<" "<<H<< endl<< endl;
	
	//Draw lines between the corners of the mapped object in the scene
	line(img_matches, scene_corners[0], scene_corners[1], Scalar(0,255,0),2);
	line(img_matches, scene_corners[1], scene_corners[2], Scalar(0,255,0),2);
	line(img_matches, scene_corners[2], scene_corners[3], Scalar(0,255,0),2);
	line(img_matches, scene_corners[3], scene_corners[0], Scalar(0,255,0),2);

	string filename="/home/iveta/comicbot_ws/src/comicbot/images/result.png";
	imwrite(filename,img_matches);
}



void user_callback(const std_msgs::UInt8& msg){
	
	user_input= msg.data;
	//ROS_INFO("%d",msg.data);
	
	VideoCapture cap(0); // open the default camera
	if(!cap.isOpened()){// check if we succeeded
	
		printf("Camera not found");
		return;
	}
	//cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
	//cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

	cap.set(CV_CAP_PROP_FPS,10);
	cap.set(CV_CAP_PROP_FRAME_WIDTH,1920);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,1080);
	
	//cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
	//cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

	Mat color;
	cap >> color; // get a new frame from cameraK
	
	
	//show on screen and save the image taken
	//namedWindow("image",CV_WINDOW_AUTOSIZE);
	//imshow("image",color);
	imwrite("/home/iveta/comicbot_ws/src/comicbot/images/misumi_full.png",color);
	//waitKey(0);
	//destroyWindow("image");
	
	resize(color, color, Size(), COMPRESSION_CONST, COMPRESSION_CONST, CV_INTER_AREA);
	
	


	//Keypoint detection
	SiftFeatureDetector detector;
	vector<KeyPoint> keypoints;
	detector.detect(color, keypoints);

	// Draw keypoints on the image taken, show and save.
	Mat output;
	drawKeypoints(color, keypoints, output);
	//namedWindow("sift_keypoints",CV_WINDOW_AUTOSIZE);
	//imshow("sift_keypoints",output);
	imwrite("/home/iveta/comicbot_ws/src/comicbot/images/sift_keypoints.jpg", output);
	//waitKey(0);
	//destroyWindow("sift_keypoints");

	//Feature extraction
	Mat descriptors;
	SiftDescriptorExtractor extractor;
	extractor.compute(color, keypoints, descriptors);
	printf("Image:%zd keypoints were found.\n", keypoints.size());

	//find the right page from the comic database
	std::vector< DMatch > good_matches;
	vector<KeyPoint> keypoints2;
	int best_page;
	recognize_page(descriptors, best_page, keypoints2, good_matches);	 //returns the double-page number or 0 if not recognized
	if (best_page == 0) {
		status=0;
		printf("Not good enough matches to recognize the page! \n");
		printf("Please place the comic in front of the camera or adjust the light. \n");
		return; //end of main
	}
	
	printf("This is a double-page number %d (pages %d - %d) \n",
					best_page, (best_page-1)*2, (best_page-1)*2+1);
	page=(best_page-1)*2;

	//Draw the keypoint matches
	Mat img_matches;
	std::ostringstream os;
	os << std::setw( 4 ) << std::setfill( '0' ) << best_page;
	string filename = "/home/iveta/comicbot_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
	Mat color2=imread(filename,1);
	drawMatches(color, keypoints, color2, keypoints2, good_matches, img_matches,
			 Scalar::all(-1), Scalar::all(-1),vector<char>(),
			 DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

	//Localize the comic book in the scene
	vector<Point2f> scene_corners(4);
	localize(keypoints, keypoints2, good_matches, img_matches, color2,
			scene_corners);

	//Show detected matches and the localized book
	//imshow( "Good Matches and Object detection", img_matches );
	//waitKey(0);

	////////////////////COMICBOOK POSE ESTIMATION////////////
	Mat K(3,3,CV_32FC1,Scalar::all(0.0));
	K.at<float>(0,0) = COMPRESSION_CONST*ALPHA_X;
	K.at<float>(0,1) = CAM_GAMMA;
	K.at<float>(0,2) = COMPRESSION_CONST*U_0;
	K.at<float>(1,1) = COMPRESSION_CONST*ALPHA_Y;
	K.at<float>(1,2) = COMPRESSION_CONST*V_0;
	K.at<float>(2,2) = 1.0;
	
	Mat DIST_COEFF(5,1,CV_32FC1,Scalar::all(0.0));
	DIST_COEFF.at<float>(0,0) = DIST_COEFF_ARRAY[0];
	DIST_COEFF.at<float>(0,1) = DIST_COEFF_ARRAY[1];
	DIST_COEFF.at<float>(0,2) = DIST_COEFF_ARRAY[2];
	DIST_COEFF.at<float>(0,3) = DIST_COEFF_ARRAY[3];
	DIST_COEFF.at<float>(0,4) = DIST_COEFF_ARRAY[4];

	//defines the dimensions of the comic book in m
	vector<Point3f> object_corners_realsize(4);
	object_corners_realsize= {{0.0,0.0,0.0},{340.0,0.0,0.0},{340.0,
					260.0,0.0},{0.0,260.0,0.0}};
	
	Mat rvec;
	Mat tvec;
	
	/*cout<<" solvePnP input: "<<endl;

	cout<<" object_corners_realsize "<<endl<<" "<<object_corners_realsize<< endl<< endl;
	cout<<" scene_corners "<<endl<<" "<<scene_corners<< endl<< endl;
	cout<<" K "<<endl<<" "<<K<< endl<< endl;
	cout<<" DIST_COEFF "<<endl<<" "<< DIST_COEFF << endl<< endl;
	*/
	solvePnP((Mat)object_corners_realsize,(Mat)scene_corners, K,
	                     DIST_COEFF, rvec, tvec, false, CV_P3P);
	
	

	//cout<<"In camera frame (solvePnP output): "<<endl;
	//cout<<" rvec "<<endl<<" "<<rvec<< endl<< endl;
	//cout<<" tvec "<<endl<<" "<<tvec<< endl << endl;


	//Transformation matrix from the camera coordinate system to the arm coordinate system
	double T_cameraArm_data[4][4]=
			{{-0.07078949252074182, -1.027491756465841, -0.281391063902429, 0.0219324656805891},
			  {-0.818345546124267, -0.0149522490527997, 0.5132081650491778, 0.1796533703528992},
			  {0.5715795326208233, -0.076098367281424, 0.9329519524914147, 0.003485282255525468},
			  {0, 0, 0, 1}};
	


	Mat T_cameraArm(4,4,CV_64FC1, T_cameraArm_data);
	//cout<<" T_cameraArm "<<endl<<" "<<T_cameraArm<< endl << endl;

	//cout<<"T_cameraArm inverse"<< endl<<" "<<T_cameraArm.inv()<< endl<< endl;

	//expresses the corners in the camera coordinate system
	//only corners 0 (pick backward) and 1 (pick forward) will be actually needed

	double data0[3]={object_corners_realsize[0].x,object_corners_realsize[0].y,0.0};
	double data1[3]={object_corners_realsize[1].x,object_corners_realsize[1].y,0.0};
	double data2[3]={object_corners_realsize[2].x,object_corners_realsize[2].y,0.0};
	double data3[3]={object_corners_realsize[3].x,object_corners_realsize[3].y,0.0};

	Mat D0=Mat(3, 1, CV_64F, data0);
	Mat D1=Mat(3, 1, CV_64F, data1);
	Mat D2=Mat(3, 1, CV_64F, data2);
	Mat D3=Mat(3, 1, CV_64F, data3);

	//cout<<" D0 "<<endl<<" "<< D0<< endl << endl;
	//cout<<" D1 "<<endl<<" "<< D1<< endl << endl;
	//cout<<" D2 "<<endl<<" "<< D2<< endl << endl;
	//cout<<" D3 "<<endl<<" "<< D3<< endl << endl;

	Mat R;
	Rodrigues(rvec,R); //transforms the rotational vector to rotational matrix
	//cout<<" R "<<endl<<" "<<R<< endl << endl;

	Mat corner0_cameracoord = (R * D0 + tvec)*0.001;
	Mat corner1_cameracoord = (R * D1 + tvec)*0.001;
	Mat corner2_cameracoord = (R * D2 + tvec)*0.001;
	Mat corner3_cameracoord = (R * D3 + tvec)*0.001;

	corner0_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
	corner1_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
	corner2_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
	corner3_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));

	
	/*
	cout<<" Corner 0 in the camera coordinate system "<<endl<<" "
			<< corner0_cameracoord<< endl << endl;
	cout<<" Corner 1 in the camera coordinate system "<<endl<<" "
				<< corner1_cameracoord<< endl << endl;
	cout<<" Corner 2 in the camera coordinate system "<<endl<<" "
				<< corner2_cameracoord<< endl << endl;
	cout<<" Corner 3 in the camera coordinate system "<<endl<<" "
				<< corner3_cameracoord<< endl << endl;
	*/
	Mat corner0_armcoord = T_cameraArm.inv() * corner0_cameracoord;
	Mat corner1_armcoord = T_cameraArm.inv() * corner1_cameracoord;
	Mat corner2_armcoord = T_cameraArm.inv() * corner2_cameracoord;
	Mat corner3_armcoord = T_cameraArm.inv() * corner3_cameracoord;

	cout<<" Corner 0 in the arm coordinate system "<<endl<<" "
				<< corner0_armcoord<< endl << endl;
	cout<<" Corner 1 in the arm coordinate system "<<endl<<" "
				<< corner1_armcoord<< endl << endl;
	//cout<<" Corner 2 in the arm coordinate system "<<endl<<" "
	//			<< corner2_armcoord<< endl << endl;
	//cout<<" Corner 3 in the arm coordinate system "<<endl<<" "
	//			<< corner3_armcoord<< endl << endl;
	
	corners[0][0]=corner0_armcoord.at<double>(0)*1000;
	corners[0][1]=corner0_armcoord.at<double>(1)*1000;
	corners[1][0]=corner1_armcoord.at<double>(0)*1000;
	corners[1][1]=corner1_armcoord.at<double>(1)*1000;
	corners[2][0]=corner2_armcoord.at<double>(0)*1000;
	corners[2][1]=corner2_armcoord.at<double>(1)*1000;
	corners[3][0]=corner3_armcoord.at<double>(0)*1000;
	corners[3][1]=corner3_armcoord.at<double>(1)*1000;
	
	/*
	ROS_INFO("%f", corners[0][0]);
	ROS_INFO("%f", corners[0][1]);
	ROS_INFO("%f", corners[1][0]);
	ROS_INFO("%f", corners[1][1]);
	ROS_INFO("%f", corners[2][0]);
	ROS_INFO("%f", corners[2][1]);
	ROS_INFO("%f", corners[3][0]);
	ROS_INFO("%f", corners[3][1]);
	*/

	//destroyWindow("Good Matches and Object detection");

	status=1;
}

void move_callback(const std_msgs::Bool& msg){
	//see what page are we on now
	VideoCapture cap(0); // open the default camera
	if(!cap.isOpened()){// check if we succeeded
	
		printf("Camera not found");
		return;
	}
	//cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
	//cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

	cap.set(CV_CAP_PROP_FPS,10);
	cap.set(CV_CAP_PROP_FRAME_WIDTH,1920);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,1080);
	
	//cout << "Frame width: " << cap.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
	//cout << "Frame height: " << cap.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;

	Mat color;
	cap >> color; // get a new frame from cameraK
	
	
	//show on screen and save the image taken
	//namedWindow("image",CV_WINDOW_AUTOSIZE);
	//imshow("image",color);
	imwrite("/home/iveta/comicbot_ws/src/comicbot/images/misumi_full.png",color);
	//waitKey(0);
	//destroyWindow("image");
	
	resize(color, color, Size(), COMPRESSION_CONST, COMPRESSION_CONST, CV_INTER_AREA);
	
	//Keypoint detection
	SiftFeatureDetector detector;
	vector<KeyPoint> keypoints;
	detector.detect(color, keypoints);

	// Draw keypoints on the image taken, show and save.
	Mat output;
	drawKeypoints(color, keypoints, output);
	//namedWindow("sift_keypoints",CV_WINDOW_AUTOSIZE);
	//imshow("sift_keypoints",output);
	imwrite("/home/iveta/comicbot_ws/src/comicbot/images/sift_keypoints.jpg", output);
	//waitKey(0);
	//destroyWindow("sift_keypoints");

	//Feature extraction
	Mat descriptors;
	SiftDescriptorExtractor extractor;
	extractor.compute(color, keypoints, descriptors);
	//printf("Image 1:%zd keypoints were found.\n", keypoints.size());

	//find the right page from the comic database
	std::vector< DMatch > good_matches;
	vector<KeyPoint> keypoints2;
	int best_page;
	recognize_page(descriptors, best_page, keypoints2, good_matches);	 //returns the double-page number or 0 if not recognized
	if (best_page == 0) {
		status=0;
		printf("Not good enough matches to recognize the page! \n");
		printf("Please place the comic in front of the camera or adjust the light. \n");
		return; //end of callback
	}
	
	printf("This is a double-page number %d (pages %d - %d) \n",
					best_page, (best_page-1)*2, (best_page-1)*2+1);
	
	
	if ((best_page-1)*2==page){ //same page as with the last attempt
		
		if((attempt==1)||(attempt==2)){

			sound_output=2;
			//ROS_INFO("%d",msg.data);
			cout<<"Page has not been turned correctly.Trying to turn the page next time"<< endl;
			attempt++; //we already tried to turn the page
			printf("This is a double-page number %d (pages %d - %d) \n",
								best_page, (best_page-1)*2, (best_page-1)*2+1);
	
			//Draw the keypoint matches
			Mat img_matches;
			std::ostringstream os;
			os << std::setw( 4 ) << std::setfill( '0' ) << best_page;
			string filename = "/home/iveta/comicbot_ws/src/comicbot/images/comics_scan/mickey_" + os.str() + "_compressed.jpg";
			Mat color2=imread(filename,1);
			drawMatches(color, keypoints, color2, keypoints2, good_matches, img_matches,
					 Scalar::all(-1), Scalar::all(-1),vector<char>(),
					 DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
	
			//Localize the comic book in the scene
			vector<Point2f> scene_corners(4);
			localize(keypoints, keypoints2, good_matches, img_matches, color2,
					scene_corners);
	
			//Show detected matches and the localized book
			//imshow( "Good Matches and Object detection", img_matches );
			//waitKey(0);
	
			////////////////////COMICBOOK POSE ESTIMATION////////////
			Mat K(3,3,CV_32FC1,Scalar::all(0.0));
			K.at<float>(0,0) = COMPRESSION_CONST*ALPHA_X;
			K.at<float>(0,1) = CAM_GAMMA;
			K.at<float>(0,2) = COMPRESSION_CONST*U_0;
			K.at<float>(1,1) = COMPRESSION_CONST*ALPHA_Y;
			K.at<float>(1,2) = COMPRESSION_CONST*V_0;
			K.at<float>(2,2) = 1.0;
			
			Mat DIST_COEFF(5,1,CV_32FC1,Scalar::all(0.0));
			DIST_COEFF.at<float>(0,0) = DIST_COEFF_ARRAY[0];
			DIST_COEFF.at<float>(0,1) = DIST_COEFF_ARRAY[1];
			DIST_COEFF.at<float>(0,2) = DIST_COEFF_ARRAY[2];
			DIST_COEFF.at<float>(0,3) = DIST_COEFF_ARRAY[3];
			DIST_COEFF.at<float>(0,4) = DIST_COEFF_ARRAY[4];
	
			//defines the dimensions of the comic book in m
			vector<Point3f> object_corners_realsize(4);
			object_corners_realsize= {{0.0,0.0,0.0},{340.0,0.0,0.0},{340.0,
							260.0,0.0},{0.0,260.0,0.0}};
			
			Mat rvec;
			Mat tvec;
			/*
			cout<<" solvePnP input: "<<endl;
	
			cout<<" object_corners_realsize "<<endl<<" "<<object_corners_realsize<< endl<< endl;
			cout<<" scene_corners "<<endl<<" "<<scene_corners<< endl<< endl;
			cout<<" K "<<endl<<" "<<K<< endl<< endl;
			cout<<" DIST_COEFF "<<endl<<" "<< DIST_COEFF << endl<< endl;
			*/
			solvePnP((Mat)object_corners_realsize,(Mat)scene_corners, K,
			                     DIST_COEFF, rvec, tvec, false, CV_P3P);
	
	
			//cout<<"In camera frame (solvePnP output): "<<endl;
			//cout<<" rvec "<<endl<<" "<<rvec<< endl<< endl;
			//cout<<" tvec "<<endl<<" "<<tvec<< endl << endl;
	
	
			//Transformation matrix from the camera coordinate system to the arm coordinate system
			
			double T_cameraArm_data[4][4]=
					{{-0.07078949252074182, -1.027491756465841, -0.281391063902429, 0.0219324656805891},
					  {-0.818345546124267, -0.0149522490527997, 0.5132081650491778, 0.1796533703528992},
					  {0.5715795326208233, -0.076098367281424, 0.9329519524914147, 0.003485282255525468},
					  {0, 0, 0, 1}};
			
	
	
			Mat T_cameraArm(4,4,CV_64FC1, T_cameraArm_data);
			//cout<<" T_cameraArm "<<endl<<" "<<T_cameraArm<< endl << endl;
	
			//cout<<"T_cameraArm inverse"<< endl<<" "<<T_cameraArm.inv()<< endl<< endl;
	
			//expresses the corners in the camera coordinate system
			//only corners 0 (pick backward) and 1 (pick forward) will be actually needed
	
			double data0[3]={object_corners_realsize[0].x,object_corners_realsize[0].y,0.0};
			double data1[3]={object_corners_realsize[1].x,object_corners_realsize[1].y,0.0};
			double data2[3]={object_corners_realsize[2].x,object_corners_realsize[2].y,0.0};
			double data3[3]={object_corners_realsize[3].x,object_corners_realsize[3].y,0.0};
	
			Mat D0=Mat(3, 1, CV_64F, data0);
			Mat D1=Mat(3, 1, CV_64F, data1);
			Mat D2=Mat(3, 1, CV_64F, data2);
			Mat D3=Mat(3, 1, CV_64F, data3);
	
			//cout<<" D0 "<<endl<<" "<< D0<< endl << endl;
			//cout<<" D1 "<<endl<<" "<< D1<< endl << endl;
			//cout<<" D2 "<<endl<<" "<< D2<< endl << endl;
			//cout<<" D3 "<<endl<<" "<< D3<< endl << endl;
	
			Mat R;
			Rodrigues(rvec,R); //transforms the rotational vector to rotational matrix
			//cout<<" R "<<endl<<" "<<R<< endl << endl;
	
			Mat corner0_cameracoord = (R * D0 + tvec)*0.001;
			Mat corner1_cameracoord = (R * D1 + tvec)*0.001;
			Mat corner2_cameracoord = (R * D2 + tvec)*0.001;
			Mat corner3_cameracoord = (R * D3 + tvec)*0.001;
	
			corner0_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
			corner1_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
			corner2_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
			corner3_cameracoord.push_back(Mat(1,1,CV_64FC1, Scalar(1.0)));
	
			
		/*
			cout<<" Corner 0 in the camera coordinate system "<<endl<<" "
					<< corner0_cameracoord<< endl << endl;
			cout<<" Corner 1 in the camera coordinate system "<<endl<<" "
						<< corner1_cameracoord<< endl << endl;
			cout<<" Corner 2 in the camera coordinate system "<<endl<<" "
						<< corner2_cameracoord<< endl << endl;
			cout<<" Corner 3 in the camera coordinate system "<<endl<<" "
						<< corner3_cameracoord<< endl << endl;
	*/
			Mat corner0_armcoord = T_cameraArm.inv() * corner0_cameracoord;
			Mat corner1_armcoord = T_cameraArm.inv() * corner1_cameracoord;
			Mat corner2_armcoord = T_cameraArm.inv() * corner2_cameracoord;
			Mat corner3_armcoord = T_cameraArm.inv() * corner3_cameracoord;
	/*
			cout<<" Corner 0 in the arm coordinate system "<<endl<<" "
						<< corner0_armcoord<< endl << endl;
			cout<<" Corner 1 in the arm coordinate system "<<endl<<" "
						<< corner1_armcoord<< endl << endl;
			cout<<" Corner 2 in the arm coordinate system "<<endl<<" "
						<< corner2_armcoord<< endl << endl;
			cout<<" Corner 3 in the arm coordinate system "<<endl<<" "
						<< corner3_armcoord<< endl << endl;
	*/		
			corners[0][0]=corner0_armcoord.at<double>(0)*1000;
			corners[0][1]=corner0_armcoord.at<double>(1)*1000;
			corners[1][0]=corner1_armcoord.at<double>(0)*1000;
			corners[1][1]=corner1_armcoord.at<double>(1)*1000;
			corners[2][0]=corner2_armcoord.at<double>(0)*1000;
			corners[2][1]=corner2_armcoord.at<double>(1)*1000;
			corners[3][0]=corner3_armcoord.at<double>(0)*1000;
			corners[3][1]=corner3_armcoord.at<double>(1)*1000;
			
			/*
			ROS_INFO("%f", corners[0][0]);
			ROS_INFO("%f", corners[0][1]);
			ROS_INFO("%f", corners[1][0]);
			ROS_INFO("%f", corners[1][1]);
			ROS_INFO("%f", corners[2][0]);
			ROS_INFO("%f", corners[2][1]);
			ROS_INFO("%f", corners[3][0]);
			ROS_INFO("%f", corners[3][1]);
			*/
			destroyWindow("Good Matches and Object detection");
	
			status=1;
		}
		else{
			attempt=1;
			cout<<"Page can't be turned, sorry! Please turn the page yourself."<< endl;
			sound_output=3;
			
		}
	} 
	else if( (((best_page-1)*2)==page+2) ||  (((best_page-1)*2)==page-2)  ){
		cout<<"Page has been turned correctly."<< endl;
		sound_output=1;
		attempt=1;
	}
	else{
		cout<<"Wrongy turned page! Please manually check what's wrong."<< endl;
		attempt=1;
	}
	
}



int main(int argc, char **argv){

	std_msgs::Float64MultiArray pos;
	pos.data.clear();
	pos.data.resize(21);
	
	std_msgs::UInt8 sound_info;
	
	
	ros::init(argc, argv, "vision_node");
	ros::NodeHandle n;
	
	ros::Subscriber sub = n.subscribe("user_topic", 1, user_callback);
	ros::Subscriber sub2 = n.subscribe("move_topic", 1, move_callback);
	ros::Publisher 	pub = n.advertise<std_msgs::Float64MultiArray>("vision_topic", 1);
	ros::Publisher 	pub2 = n.advertise<std_msgs::UInt8>("vision_topic_sound", 1);
	ros::Rate loop_rate(20);
	
	
	while (ros::ok()){
	
		if(sound_output!=0){
			sound_info.data=sound_output;
			pub2.publish(sound_info);
			sound_output=0;
		}
		
		if(status==1){ //the page of the comic book was recognised
			status=0;
			pos.data[0] = 1.0; //tell the move_node that the page was correctly recognised
			pos.data[1] = corners[0][0]; //returns the coordinates of the coorners
			pos.data[2] = corners[0][1];
			pos.data[3] = corners[1][0];
			pos.data[4] = corners[1][1];
			pos.data[5] = corners[2][0];
			pos.data[6] = corners[2][1];
			pos.data[7] = corners[3][0];
			pos.data[8] = corners[3][1];
			pos.data[9]= user_input;
			pos.data[10]= page; //returns the number of the left page of the double-page
			pos.data[11]=H_ROS[0]; //H_comicCamera transformation matrix, so that T_comicProjector can be calculated within the projection node
			pos.data[12]=H_ROS[1];
			pos.data[13]=H_ROS[2]; 
			pos.data[14]=H_ROS[3];
			pos.data[15]=H_ROS[4];
			pos.data[16]=H_ROS[5];
			pos.data[17]=H_ROS[6];
			pos.data[18]=H_ROS[7];
			pos.data[19]=H_ROS[8];
			pos.data[20]=attempt; //for how many time we are trying to turn this page
			pub.publish(pos);
		}
		else{

		}
		
		ros::spinOnce();
		loop_rate.sleep();
	}		
	return 0;
}
